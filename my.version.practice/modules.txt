# The absolute least amount of information you could provide for foo module with description would be
# foo||||||foo description
# This above is probably the best, or certainly the simplest way to prescribe your modules
#
# Or, you could get a foo module without a description by using just
# foo
# But you probably wouldn't like that because we'll make up a jive description for you and put it in there
#
# In the following lines, the values '0.0.1-SNAPSHOT' and 'jar' and 'standard' are all redundant because these are what would be specified if you left them empty
# In other words, these next two lines are read in as the same
# foo|foo|foo|0.0.1-SNAPSHOT|jar|standard|Foo description
# foo||||||Foo description
#
# The first field is special and must be lower case no dashes dots etc if you want a more complex name for your project do something like this
# foo|My Foo Module|com.mycompany.myapp.foo||||My foo module description here
#
# Happy Module Generating!
#
one|One|my.version.practice.one|0.0.2-SNAPSHOT|jar||one description
two|Two|my.version.practice.two||||foo description
five|Five|my.version.practice.five|0.0.4-SNAPSHOT|jar||mandatory description
six|Six|my.version.practice.six|0.0.5-SNAPSHOT|jar||six description
api||my.version.practice.api|0.0.6-SNAPSHOT|jar||api description
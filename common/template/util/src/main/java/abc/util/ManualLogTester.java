package @REPLACE_PACKAGE@.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Run this from the main method from within the IDE, observe that the log file
 * was written, and also where it was written relative to the project. Refresh
 * the logs project and look for the file slfforj.log to verify that an entry
 * was made properly
 * 
 * @author petecarapetyan
 * 
 */
public class ManualLogTester {
	private Logger log = LoggerFactory
			.getLogger(" @REPLACE_PACKAGE@.ManualLogTester");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ManualLogTester manualLogTester = new ManualLogTester();
		System.out.println("THIS IS " + manualLogTester.getClass().getName());
		manualLogTester.log
				.warn("This allows you to check and see if the logger is running properly");
	}
}

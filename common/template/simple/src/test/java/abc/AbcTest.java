package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a;
import @REPLACE_PACKAGE@.@REPLACE_HASH@.@REPLACE_CLASS_NAME@a;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class @REPLACE_CLASS_NAME@aTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public @REPLACE_CLASS_NAME@aTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( @REPLACE_CLASS_NAME@aTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}

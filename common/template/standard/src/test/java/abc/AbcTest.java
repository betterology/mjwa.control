package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a;
import @REPLACE_PACKAGE@.@REPLACE_HASH@.@REPLACE_CLASS_NAME@a;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 
 * @author petecarapetyan
 *
 */
@Component
public class @REPLACE_CLASS_NAME@aTest extends Abstract@REPLACE_CLASS_NAME@Test{
	@Autowired
	I@REPLACE_CLASS_NAME@a @REPLACE_HASH@a;


	@Before
	public void setUp() throws Exception {
		System.out.println("Yes, setup ran");
	}

    /**
     * 
     */
	@Test
    public void testBeanIsABean(){
		assertNotNull(@REPLACE_HASH@a);
    }

	@Test
	public void myTest() throws Exception {
		assertTrue(true);
		assertEquals("hi 6", @REPLACE_HASH@a.@REPLACE_HASH@a1a(6));
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Yes, tearDown ran");
	}


}

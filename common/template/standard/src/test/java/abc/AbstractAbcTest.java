package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** @author petecarapetyan */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/@REPLACE_HASH@-bundle-context.xml" }, inheritLocations = true)
public abstract class Abstract@REPLACE_CLASS_NAME@Test{
}
package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Run this from the main method from within the IDE, observe that the log file
 * was written, and also where it was written relative to the project. Refresh
 * the logs project and look for the file slfforj.log to verify that an entry
 * was made properly
 * 
 * @author petecarapetyan
 * 
 */
public class ManualLogTester {
	private Logger log = LoggerFactory
			.getLogger(" @REPLACE_PACKAGE@.@REPLACE_HASH@.ManualLogTester");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ManualLogTester manualLogTester = new ManualLogTester();
		System.out.println("THIS IS " + manualLogTester.getClass().getName());
		manualLogTester.log
				.warn("This allows you to compare how it's going to log inside the @REPLACE_HASH@ module");
	}
}

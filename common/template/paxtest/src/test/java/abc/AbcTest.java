package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a;
import @REPLACE_PACKAGE@.@REPLACE_HASH@.@REPLACE_CLASS_NAME@a;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.ops4j.pax.exam.CoreOptions.bundle;
import static org.ops4j.pax.exam.CoreOptions.equinox;
import static org.ops4j.pax.exam.CoreOptions.frameworks;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.provision;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.Abstract@REPLACE_CLASS_NAME@Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 
 * @author petecarapetyan
 *
 */
public class @REPLACE_CLASS_NAME@aTest extends Abstract@REPLACE_CLASS_NAME@Test{
	@Inject
	private BundleContext bundleContext;

	private I@REPLACE_CLASS_NAME@a @REPLACE_HASH@a;

	@Configuration
	public static Option[] configuration() {
		return options(
				frameworks(equinox()),
				profile("spring.dm"),
				provision(
						mavenBundle().groupId("@REPLACE_PACKAGE@")
								.artifactId("@REPLACE_HASH@")
								.version("0.0.@REPLACE_DATE_YYMMDD@"),
						mavenBundle().groupId("ch.qos.logback")
								.artifactId("logback-classic")
								.version("0.9.24"),
						mavenBundle().groupId("ch.qos.logback")
								.artifactId("logback-core").version("0.9.24"),
						mavenBundle().groupId("org.slf4j")
								.artifactId("slf4j-api").version("1.6.1")				
						// ,mavenBundle().groupId("").artifactId("")
						// .version("")				
				));
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Yes, setup ran");
		initialize();
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Yes, tearDown ran");
	}

	@Test
	public void myTest() throws Exception {
		assertTrue(true);
		assertNotNull(bundleContext);
		assertNotNull(@REPLACE_HASH@a);
	}
	/**
	 *
	 */
	@Test
	public void testBeanIsABean() {
		assertNotNull(@REPLACE_HASH@a);
		assertEquals("hi 6", @REPLACE_HASH@a.@REPLACE_HASH@a1a(6));
	}

	private void initialize() {
		ServiceTracker tracker = new ServiceTracker(bundleContext,
				@REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a.class.getName(), null);
		tracker.open();
		try {
			@REPLACE_HASH@a = (I@REPLACE_CLASS_NAME@a) tracker.waitForService(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		tracker.close();
	}

	

}

package @REPLACE_PACKAGE@.@REPLACE_HASH@.internal;

import javax.annotation.PostConstruct;

import @REPLACE_PACKAGE@.smg.ISmga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * example code for how a class might call an external bean such as from another project
 */
@Component
public class MySampleClient {
	private ISmga smga;

	@Autowired
	MySampleClient(ISmga smga) {
		this.smga = smga;
	}

	@PostConstruct
	public void init() {
		System.out
				.println("PostConstruct ran and the init method fired off this system out at "
						+ smga.smga1c());
	}

}

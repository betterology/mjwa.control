package @REPLACE_PACKAGE@.@REPLACE_HASH@.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This allows you to run a test of the logging MANUALLY, outside JUnit. This it
 * is NOT a regression test.
 * 
 * After running this main method, usually from within the IDE, observe that the
 * log file was written, and also where it was written relative to the project.
 * Look outside this project or jar - the file is ./../logs/@REPLACE_HASH@.log
 * such as, on Pete's box c:/work/modules/logs/@REPLACE_HASH@.log
 * 
 * @author petecarapetyan
 *
 */
public class TestLoggingOutsideJunitManually {
	private Logger log = LoggerFactory.getLogger("Foo");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestLoggingOutsideJunitManually().log.warn("This allows you to check and see if the logger is running properly");
	}

}

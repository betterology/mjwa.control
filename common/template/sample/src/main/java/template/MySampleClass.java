package @REPLACE_PACKAGE@.@REPLACE_HASH@.internal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * MySampleClass is really a StringUtils class adapted here for a 
 * sample class, to give the user plenty of real code to see how 
 * an internal OSGI class (which is obfuscated later by ProGuard)
 * relates to it's exported wrapper class, which is never obfuscated
 * and thus retains it's signature.
 * 
 * Please note the one to one relationship between the methods in this class
 * and the methods in the @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a exported class.
 * 
 * This class is intended to be deleted from this project, once the 
 * developer of the project has learned how the above relationship
 * works and how to repeat same.
 * 
 * @author petecarapetyan
 *
 */
public class MySampleClass {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	public String getFrontFilledFixedLengthString(String string, char backFill,
			int length, boolean errorOnWidth, boolean errorIfNotNumeric) {
		String returnValue = string;
		if (null == string) {
			throw new IllegalArgumentException("cannot backfill a null value");
		}
		if (errorIfNotNumeric) {
			try {
				double temp = Double.parseDouble(string);
			} catch (NumberFormatException e) {
				throw new NumberFormatException(
						"Was asked to throw a NumberFormatException if given a value that is not numeric, which '"
								+ string + "' seems to be");
			}
		}
		if (errorOnWidth) {
			if (returnValue.length() > length) {
				throw new IllegalArgumentException(
						"Was asked to throw an error if given a string longer than "
								+ length + " charachters");
			}
		}
		while (returnValue.length() < length) {
			returnValue = backFill + returnValue;
		}
		return returnValue;
	}

	public String dotPathToSlashPath(String path) {
		return replace(path, ".", "/");
	}

	public void printProperties(Properties properties) {
		try {
			StringTokenizer stk = new StringTokenizer(properties.toString(),
					" ");
			String packageName = "";
			boolean firstToken = true;
			while (stk.hasMoreTokens()) {
				System.out.println(stk.nextToken());
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
		}
	}

	public String notNull(String theString) {
		if (theString == null) {
			return new String("");
		}
		return theString;
	}

	public String lastToken(String value, char delimiter) {
		StringTokenizer stk = new StringTokenizer(value, String
				.valueOf(delimiter));
		String lastToken = "";
		while (stk.hasMoreTokens()) {
			lastToken = stk.nextToken();
		}
		return lastToken;
	}

	public String getClassNameLikeFromDelimited(String var, char delimiter) {
		StringTokenizer stk = null;
		String token = null;
		String returnValue = "";
		stk = new StringTokenizer(var, String.valueOf(delimiter));
		while (stk.hasMoreTokens()) {
			token = stk.nextToken();
			token = token.substring(0, 1).toUpperCase()
					+ token.substring(1, token.length()).toLowerCase();
			returnValue = returnValue + token;
		}
		return returnValue;
	}

	public String getMethodNameLikeFromDelimited(String var, char delimiter) {
		StringTokenizer stk = null;
		String token = null;
		String returnValue = "";
		boolean first = true;
		stk = new StringTokenizer(var, String.valueOf(delimiter));
		while (stk.hasMoreTokens()) {
			token = stk.nextToken();
			if (first) {
				token = token.substring(0, token.length()).toLowerCase();
				first = false;
			} else {
				token = token.substring(0, 1).toUpperCase()
						+ token.substring(1, token.length()).toLowerCase();
			}
			returnValue = returnValue + token;
		}
		return returnValue;
	}

	public String numberToLetter(int number, boolean upperCaseFlag)
			throws IllegalArgumentException {
		if (number < 1 || number > 26) {
			throw new IllegalArgumentException(
					"The number is out of the proper range (1 to 26) to be converted to a letter.");
		}
		int modnumber = number + 9;
		char thechar = Character.forDigit(modnumber, 36);
		if (upperCaseFlag) {
			thechar = Character.toUpperCase(thechar);
		}
		return "" + thechar;
	}

	public String replace(String s, String sub, String with) {
		int c = 0;
		int i = s.indexOf(sub, c);
		if (i == -1)
			return s;
		StringBuffer buf = new StringBuffer(s.length() + with.length());
		do {
			buf.append(s.substring(c, i));
			buf.append(with);
			c = i + sub.length();
		} while ((i = s.indexOf(sub, c)) != -1);
		if (c < s.length())
			buf.append(s.substring(c, s.length()));
		return buf.toString();
	}

	public String upLow(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length()).toLowerCase();
		return start.toUpperCase() + finish;
	}

	public String upStart(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length());
		return start.toUpperCase() + finish;
	}

	public String lowStart(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length());
		return start.toLowerCase() + finish;
	}

	public String trimJavaPeteCStyle(String line) {
		String newLine = replace(line, "\t", "  ");
		while (newLine.endsWith(" ")) {
			newLine = newLine.substring(0, newLine.length() - 1);
		}
		return newLine;
	}

	public boolean trueTest(String val) {
		if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("Yes")
				|| val.equalsIgnoreCase("Y") || val.equalsIgnoreCase("checked")) {
			return true;
		}
		return false;
	}

	public String yesNoToTrueFalse(String val) {
		String yesNoToTrueFalse = null;
		if (val.equalsIgnoreCase("yes") || val.equalsIgnoreCase("Y")) {
			yesNoToTrueFalse = "true";
		} else if (val.equalsIgnoreCase("no") || val.equalsIgnoreCase("N")) {
			yesNoToTrueFalse = "false";
		}
		return yesNoToTrueFalse;
	}

	public List listFromDelimited(String values, String delimiter) {
		ArrayList valuesList = new ArrayList();
		StringTokenizer stk = new StringTokenizer(values, delimiter);
		while (stk.hasMoreTokens()) {
			valuesList.add(stk.nextToken());
		}
		return valuesList;
	}

	public boolean falseTest(String val) {
		if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("no")
				|| val.equalsIgnoreCase("n")) {
			return true;
		}
		return false;
	}

	public String booleanStringReturn(String val) {
		if (trueTest(val)) {
			return "true";
		} else if (falseTest(val)) {
			return "false";
		} else {
			return "neither";
		}
	}

	public boolean isNull(String var) {
		if (var == null || var.equalsIgnoreCase("null")
				|| var.trim().equals("") || var.length() <= 0) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<String> splitStringList(String s, int length) {
		return splitStringListDelimiter(s, length, " ");
	}

	public ArrayList<String> splitStringListDelimiter(String s, int length,
			String delimiter) {
		try {
			StringTokenizer stk = new StringTokenizer(s, delimiter);
			ArrayList<String> lines = new ArrayList<String>(2);
			StringBuffer currentLine = new StringBuffer(length);
			String insert = null;
			int lengthBuffer = 0;
			boolean done = false;
			while (stk.hasMoreTokens()) {
				insert = stk.nextToken();
				if ((lengthBuffer + insert.length()) < length) {
					currentLine.append(" " + insert);
					lengthBuffer = lengthBuffer + insert.length();
				} else {
					lengthBuffer = 0;
					lines.add(currentLine.toString().trim());
					currentLine.delete(0, currentLine.length());
					currentLine.append(insert);
					lengthBuffer = lengthBuffer + insert.length();
				}
			}
			lines.add(currentLine.toString().trim());
			return lines;
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	public String splitString(String stringToSplit, int length) {
		try {
			if (stringToSplit == null) {
				return "";
			} else if (stringToSplit.length() < 1) {
				return "";
			} else {
				StringBuffer sb = new StringBuffer(300);
				ArrayList<String> textList = splitStringList(stringToSplit,
						length);
				Iterator<String> stringIterator = textList.iterator();
				while (stringIterator.hasNext()) {
					sb.append((String) stringIterator.next() + "\n");
				}
				return sb.toString();
			}
		} catch (Exception e) {
			throw new RuntimeException("splitString(String) " + e.getMessage());
		}
	}

	public String splitStringWithDelimiter(String stringToSplit, int length,
			String delimiter) {
		try {
			if (stringToSplit == null) {
				return "";
			} else if (stringToSplit.length() < 1) {
				return "";
			} else {
				StringBuffer sb = new StringBuffer(300);
				ArrayList<String> textList = splitStringListDelimiter(
						stringToSplit, length, delimiter);
				Iterator<String> stringIterator = textList.iterator();
				while (stringIterator.hasNext()) {
					sb.append((String) stringIterator.next() + "\n");
				}
				return sb.toString();
			}
		} catch (Exception e) {
			throw new RuntimeException("splitString(String) " + e.getMessage());
		}
	}

	public String splitStringForcedDelimiter(String stringToSplit, int length,
			String delimiter) {
		String testString = null;
		try {
			if (stringToSplit == null) {
				return "";
			} else if (stringToSplit.length() < 1) {
				return "";
			} else {
				StringBuffer sb = new StringBuffer(300);
				ArrayList<String> textList = splitStringList(stringToSplit,
						length);
				Iterator<String> stringIterator = textList.iterator();
				while (stringIterator.hasNext()) {
					testString = (String) stringIterator.next();
					if (testString.length() > length) {
						testString = splitStringWithDelimiter(testString,
								length, delimiter);
					}
					sb.append(testString + "\n");
				}
				return sb.toString();
			}
		} catch (Exception e) {
			throw new RuntimeException("splitString(String) " + e.getMessage());
		}
	}

	public String randomString(int stringLength, boolean numericToo) {
		StringBuffer newString = new StringBuffer("");
		double oneNumber = 0;
		char oneChar;
		int iterations = 0;
		while ((newString.length() < stringLength) && (iterations < 200)) {
			iterations++;
			while (!okNumber(oneNumber, numericToo)) {
				oneNumber = Math.random() * 170;
			}
			oneChar = (char) new Double(oneNumber).intValue();
			newString.append(oneChar);
			oneNumber = 0;
		}

		return newString.toString();
	}

	public String randomNumberString(int stringLength) {
		StringBuffer newString = new StringBuffer("");
		double oneNumber = 0;
		char oneChar;
		int iterations = 0;
		while ((newString.length() < stringLength) && (iterations < 200)) {
			iterations++;
			while (!numberOnly(oneNumber)) {
				oneNumber = Math.random() * 170;
			}
			oneChar = (char) new Double(oneNumber).intValue();
			newString.append(oneChar);
			oneNumber = 0;
		}

		return newString.toString();
	}

	private static boolean okNumber(double x, boolean numericToo) {
		int oneNumber = new Double(x).intValue();
		if ((oneNumber >= 65) && (oneNumber <= 90)) {
			return true;
		}

		if ((oneNumber >= 48) && (oneNumber <= 57) && numericToo) {
			return true;
		}

		if ((oneNumber >= 97) && (oneNumber <= 122)) {
			return true;
		}

		return false;
	}

	private static boolean numberOnly(double x) {
		int oneNumber = new Double(x).intValue();
		if ((oneNumber >= 48) && (oneNumber <= 57)) {
			return true;
		}
		return false;
	}

	public String datePrint() {
		Date date = new Date();
		return yyyy_MM_dd(date);
	}

	public String yyMMddHHmmssPrint() {
		Date date = new Date();
		return yyMMddHHmmss(date);
	}

	public String yyyy_MM_dd(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy_MM_dd");
		return formatter.format(date);
	}

	public String yyyyDashMMDashdd(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}

	public String dd_MMM_yy(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("dd-MMM-yy");
		return formatter.format(date);
	}

	public String yyyyMMdd(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(date);
	}

	public String yyMMddHHmmss(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyMMddHHmmss");
		return formatter.format(date);
	}

	public String yyMMdd(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyMMdd");
		return formatter.format(date);
	}

	public String mMslashddslashyyyy(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("MM/dd/yyyy");
		return formatter.format(date);
	}

	public String yyyyMMddHHmmss(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		return formatter.format(date);
	}

	public String endSingleSlash(String fileFolderPath) {
		while (fileFolderPath.endsWith("/") || fileFolderPath.endsWith("\\")) {
			fileFolderPath = fileFolderPath.substring(0, fileFolderPath
					.length() - 1);
		}
		fileFolderPath = fileFolderPath + "/";
		return fileFolderPath;
	}

	public String removeLineHavingValue(String text, String value) {
		StringBuffer sb = new StringBuffer();
		try {
			StringTokenizer stk = new StringTokenizer(text, "\n");
			String lineToAppend = null;
			while (stk.hasMoreTokens()) {
				lineToAppend = stk.nextToken();
				if (lineToAppend.indexOf(value) < 0) {
					sb.append(lineToAppend + "\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
		// remove trailing \n
		return sb.toString().substring(0, sb.toString().length() - 1);
	}

	public String removeEmptyLines(String text) {
		StringBuffer sb = new StringBuffer();
		try {
			StringTokenizer stk = new StringTokenizer(text, "\n");
			String lineToAppend = null;
			while (stk.hasMoreTokens()) {
				lineToAppend = stk.nextToken();
				if (lineToAppend.trim().length() > 0) {
					sb.append(lineToAppend + "\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
		// remove trailing \n
		return sb.toString().substring(0, sb.toString().length() - 1);
	}

	public String orderLinesByAlpha(String text) {
		StringBuffer sb = new StringBuffer();
		ArrayList<String> linesList = new ArrayList<String>();
		SortedSet<String> orderedLinesList = null;
		try {
			StringTokenizer stk = new StringTokenizer(text, "\n");
			String lineToAppend = null;
			while (stk.hasMoreTokens()) {
				lineToAppend = stk.nextToken();
				if (lineToAppend.trim().length() > 0) {
					linesList.add(lineToAppend);
				}
			}
			orderedLinesList = new TreeSet<String>(linesList);
			for (Iterator<String> iter = orderedLinesList.iterator(); iter
					.hasNext();) {
				sb.append((String) iter.next() + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
		// remove trailing \n
		return sb.toString().substring(0, sb.toString().length() - 1);
	}

	public boolean asciiIsInTextRange(double x, boolean numericToo) {
		int oneNumber = new Double(x).intValue();

		if ((oneNumber >= 65) && (oneNumber <= 90)) {
			return true;
		}

		if ((oneNumber >= 48) && (oneNumber <= 57) && numericToo) {
			return true;
		}

		if ((oneNumber >= 97) && (oneNumber <= 122)) {
			return true;
		}

		return false;
	}

	public String loadConvert(String theString) {
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);

		for (int x = 0; x < len;) {
			aChar = theString.charAt(x++);
			if (aChar == '\\') {
				aChar = theString.charAt(x++);
				if (aChar == 'u') {
					// Read the xxxx
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = theString.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							throw new IllegalArgumentException(
									"Malformed \\uxxxx encoding.");
						}
					}
					outBuffer.append((char) value);
				} else {
					if (aChar == 't')
						aChar = '\t';
					else if (aChar == 'r')
						aChar = '\r';
					else if (aChar == 'n')
						aChar = '\n';
					else if (aChar == 'f')
						aChar = '\f';
					outBuffer.append(aChar);
				}
			} else {
				outBuffer.append(aChar);
			}
		}
		return outBuffer.toString();
	}

	public String shift6(String string) {
		byte[] byteArray = string.getBytes();
		byte[] newByteArray = new byte[byteArray.length];
		byte[] one = new byte[1];
		byte[] two = new byte[1];
		for (int i = 0; i < byteArray.length; i++) {
			newByteArray[i] = add6(byteArray[i]);
		}
		return new String(newByteArray);
	}

	public String shift6back(String string) {
		byte[] byteArray = string.getBytes();
		byte[] newByteArray = new byte[byteArray.length];
		byte[] one = new byte[1];
		byte[] two = new byte[1];
		for (int i = 0; i < byteArray.length; i++) {
			newByteArray[i] = subtract6(byteArray[i]);
		}
		return new String(newByteArray);
	}

	private byte subtract6(byte byteToConvert) {
		int byteValue = (int) byteToConvert;
		if (byteValue > 37) {
			byteValue = byteValue - 6;
		} else if (byteValue > 31 && byteValue < 38) {
			byteValue = byteValue + 120 - 30;
		}
		return (byte) byteValue;
	}

	private byte add6(byte byteToConvert) {
		int byteValue = (int) byteToConvert;
		if (byteValue > 121) {
			byteValue = byteValue - 120 + 30;
		} else {
			byteValue = byteValue + 6;
		}
		return (byte) byteValue;
	}

	public String getPaddedString(String stringToPad, int desiredLength,
			char paddingCharachter, boolean ifLongerReturnAnyway) {
		StringBuffer paddedResult = new StringBuffer();
		if (null == stringToPad) {
			throw new IllegalArgumentException("Cannot pad a null string");
		}
		if (stringToPad.length() > desiredLength && !ifLongerReturnAnyway) {
			throw new IllegalArgumentException("Was asked to pad a string '"
					+ stringToPad
					+ "' which is longer than the total lentgth padded of "
					+ desiredLength);
		}
		paddedResult.append(stringToPad);
		while (paddedResult.toString().length() < desiredLength) {
			paddedResult.append(paddingCharachter);
		}
		return paddedResult.toString();
	}

	public String getPrePaddedString(String stringToPad, int desiredLength,
			char paddingCharachter, boolean ifLongerReturnAnyway) {
		if (null == stringToPad) {
			throw new IllegalArgumentException("Cannot pad a null string");
		}
		if (stringToPad.length() > desiredLength && !ifLongerReturnAnyway) {
			throw new IllegalArgumentException(
					"Was asked to pad a string which is longer than the total lentgth padded");
		}
		while (stringToPad.length() < desiredLength) {
			stringToPad = paddingCharachter + stringToPad;
		}
		return stringToPad;
	}

	public String removeEndPaddingFromString(String stringToClean,
			char paddingCharachter) {
		while (stringToClean.endsWith("" + paddingCharachter)) {
			stringToClean = stringToClean.substring(0,
					stringToClean.length() - 1);
		}
		return stringToClean;
	}

	public String removeMultipleWhiteSpaceCharachters(String string) {
		string = string.trim();
		// the idea is to see if the current and next charachter are both
		// whitespaces. If they are, then the current one is thrown away. If the
		// current one is a whitespace character but not a space, it is replaced
		// with a space.
		char aChar, bChar;
		int len = string.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = string.charAt(x++);
			if (x <= len - 1) {
				bChar = string.charAt(x);
				if (isWhiteSpace(aChar) && isWhiteSpace(bChar)) {
					// do nothing
				} else if (isWhiteSpace(aChar) && !isWhiteSpace(bChar)) {
					// System.out.print('_');
					outBuffer.append(' ');
				} else if (!isWhiteSpace(aChar)) {
					// System.out.print(aChar);
					outBuffer.append(aChar);
				}
			} else {
				outBuffer.append(aChar);
			}
		}
		return outBuffer.toString();
	}

	public boolean isWhiteSpace(char char1) {
		if (char1 == '\n' || char1 == '\t' || char1 == ' ' || char1 == '\r'
				|| char1 == '\f') {
			return true;
		}
		return false;
	}

}

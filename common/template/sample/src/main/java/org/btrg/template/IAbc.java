package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;


/**
 * Example obfuscated class. After running ProGuard, this class, decompiled, should not be easy to understand.
 */
public interface I@REPLACE_CLASS_NAME@a{

	/**
	 * getFrontFilledFixedLengthString
	 * 
	 * @param string
	 * @param backFill
	 * @param length
	 * @param errorOnWidth
	 * @param errorIfNotNumeric
	 * @return
	 */
	public abstract String @REPLACE_HASH@aa(String @REPLACE_HASH@aaa, char @REPLACE_HASH@aab, int @REPLACE_HASH@aac,
			boolean @REPLACE_HASH@aad, boolean @REPLACE_HASH@aae);

	/**
	 * dotPathToSlashPath
	 * 
	 * @param String
	 *            such as com.myurl.folder
	 * @return String such as com/myurl/folder
	 */
	public abstract String @REPLACE_HASH@ab(String @REPLACE_HASH@aba);

	/**
	 * printProperties
	 * 
	 * @param properties
	 */
	public abstract void @REPLACE_HASH@ac(Properties @REPLACE_HASH@aca);

	/**
	 * notNull
	 * 
	 * @param theString
	 * @return
	 */
	public abstract String @REPLACE_HASH@ad(String @REPLACE_HASH@ada);

	/**
	 * lastToken
	 * 
	 * @param value
	 * @param delimiter
	 * @return
	 */
	public abstract String @REPLACE_HASH@ae(String @REPLACE_HASH@aea, char @REPLACE_HASH@aeb);

	/**
	 * getClassNameLikeFromDelimited
	 * 
	 * @param var
	 * @param delimiter
	 * @return
	 */
	public abstract String @REPLACE_HASH@af(String @REPLACE_HASH@afa, char @REPLACE_HASH@afb);

	/**
	 * getMethodNameLikeFromDelimited
	 * 
	 * @param var
	 * @param delimiter
	 * @return
	 */
	public abstract String @REPLACE_HASH@ag(String @REPLACE_HASH@aga, char @REPLACE_HASH@agb);

	/**
	 * numberToLetter
	 * 
	 * @param number
	 * @param upperCaseFlag
	 * @return
	 * @throws Exception
	 */
	public abstract String @REPLACE_HASH@ah(int @REPLACE_HASH@aha, boolean @REPLACE_HASH@ahb) throws Exception;

	/**
	 * replace
	 * 
	 * @param s
	 * @param sub
	 * @param with
	 * @return
	 */
	public abstract String @REPLACE_HASH@ai(String @REPLACE_HASH@aia, String @REPLACE_HASH@aib, String @REPLACE_HASH@aic);

	/**
	 * upLow
	 * 
	 * @param var
	 * @return
	 */
	public abstract String @REPLACE_HASH@aj(String @REPLACE_HASH@aja);

	/**
	 * upStart
	 * 
	 * @param var
	 * @return
	 */
	public abstract String @REPLACE_HASH@ak(String @REPLACE_HASH@aka);

	/**
	 * lowStart
	 * 
	 * @param var
	 * @return
	 */
	public abstract String @REPLACE_HASH@al(String @REPLACE_HASH@ala);

	/**
	 * trimJavaPeteCStyle
	 * 
	 * @param line
	 * @return
	 */
	public abstract String @REPLACE_HASH@am(String @REPLACE_HASH@ama);


	/**
	 * trueTest
	 * 
	 * @param val
	 * @return
	 */
	public abstract boolean @REPLACE_HASH@ap(String @REPLACE_HASH@apa);

	/**
	 * yesNoToTrueFalse returns "true" if given Yes, YES, yes, Y or y <br>
	 * returns "false" if given No, NO, no, n, or N <br>
	 * returns null if given anything else
	 * 
	 * @param val
	 * @return
	 */
	public abstract String @REPLACE_HASH@aq(String @REPLACE_HASH@aqa);

	/**
	 * listFromDelimited
	 * 
	 * @param values
	 * @param delimiter
	 * @return
	 */
	public abstract List @REPLACE_HASH@ar(String @REPLACE_HASH@ara, String @REPLACE_HASH@arb);

	/**
	 * falseTest
	 * 
	 * @param val
	 * @return
	 */
	public abstract boolean @REPLACE_HASH@as(String @REPLACE_HASH@asa);

	/**
	 * booleanStringReturn
	 * 
	 * @param val
	 * @return
	 */
	public abstract String @REPLACE_HASH@at(String @REPLACE_HASH@ata);

	/**
	 * isNull
	 * 
	 * @param var
	 * @return
	 */
	public abstract boolean @REPLACE_HASH@au(String @REPLACE_HASH@aua);

	/**
	 * splitStringList
	 * 
	 * @param s
	 * @param length
	 * @return
	 */
	public abstract ArrayList<String> @REPLACE_HASH@av(String @REPLACE_HASH@ava, int @REPLACE_HASH@avb);

	/**
	 * splitStringListDelimiter
	 * 
	 * @param s
	 * @param length
	 * @param delimiter
	 * @return
	 */
	public abstract ArrayList<String> @REPLACE_HASH@aw(String @REPLACE_HASH@aw, int @REPLACE_HASH@awb,
			String @REPLACE_HASH@awc);

	/**
	 * splitString
	 * 
	 * @param stringToSplit
	 * @param length
	 * @return
	 */
	public abstract String @REPLACE_HASH@ax(String @REPLACE_HASH@aa, int @REPLACE_HASH@axb);

	/**
	 * splitStringWithDelimiter
	 * 
	 * @param stringToSplit
	 * @param length
	 * @param delimiter
	 * @return
	 */
	public abstract String @REPLACE_HASH@ay(String @REPLACE_HASH@aya, int @REPLACE_HASH@ayb, String @REPLACE_HASH@ayc);

	/**
	 * splitStringForcedDelimiter
	 * 
	 * @param stringToSplit
	 * @param length
	 * @param delimiter
	 * @return
	 */
	public abstract String @REPLACE_HASH@az(String @REPLACE_HASH@aza, int @REPLACE_HASH@azb, String @REPLACE_HASH@azc);

	/**
	 * randomString
	 * 
	 * @param stringLength
	 * @param numericToo
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1a(int @REPLACE_HASH@a1aa, boolean @REPLACE_HASH@a1ab);

	/**
	 * randomNumberString
	 * 
	 * @param stringLength
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1b(int @REPLACE_HASH@a1ba);

	/**
	 * datePrint
	 * 
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1c();

	/**
	 * yyMMddHHmmssPrint
	 * 
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1d();

	/**
	 * yyyy_MM_dd
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1e(Date @REPLACE_HASH@a1ea);

	/**
	 * yyyyDashMMDashdd
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1f(Date @REPLACE_HASH@a1fa);

	/**
	 * dd_MMM_yy
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1g(Date @REPLACE_HASH@a1ga);

	/**
	 * yyyyMMdd
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1h(Date @REPLACE_HASH@a1ha);

	/**
	 * yyMMddHHmmss
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1j(Date @REPLACE_HASH@a1ja);

	/**
	 * yyMMdd
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1k(Date @REPLACE_HASH@a1ka);

	/**
	 * MMddyyyy
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1m(Date @REPLACE_HASH@a1ma);

	/**
	 * yyyyMMddHHmmss
	 * 
	 * @param date
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1n(Date @REPLACE_HASH@a1na);

	/**
	 * endSingleSlash
	 * 
	 * @param fileFolderPath
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1o(String @REPLACE_HASH@a1o);

	/**
	 * removeLineHavingValue
	 * 
	 * @param text
	 * @param value
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1p(String @REPLACE_HASH@a1a, String @REPLACE_HASH@a1pb);

	/**
	 * removeEmptyLines
	 * 
	 * @param text
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1q(String @REPLACE_HASH@a1qa);

	/**
	 * orderLinesByAlpha
	 * 
	 * @param text
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1r(String @REPLACE_HASH@a1ra);

	/**
	 * asciiIsInTextRange
	 * 
	 * @param x
	 * @param numericToo
	 * @return
	 */
	public abstract boolean @REPLACE_HASH@a1s(double @REPLACE_HASH@a1sa, boolean @REPLACE_HASH@a1sb);

	/**
	 * loadConvert
	 * 
	 * @param theString
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1t(String @REPLACE_HASH@a1ta);

	/**
	 * shift6
	 * 
	 * @param string
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1u(String @REPLACE_HASH@a1ua);

	/**
	 * shift6back
	 * 
	 * @param string
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1v(String @REPLACE_HASH@a1va);

	/**
	 * getPaddedString
	 * 
	 * @param stringToPad
	 * @param desiredLength
	 * @param paddingCharachter
	 * @param ifLongerReturnAnyway
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1w(String @REPLACE_HASH@a1wa, int @REPLACE_HASH@a1wb, char @REPLACE_HASH@a1wc,
			boolean @REPLACE_HASH@a1wd);

	/**
	 * getPrePaddedString
	 * 
	 * @param stringToPad
	 * @param desiredLength
	 * @param paddingCharachter
	 * @param ifLongerReturnAnyway
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1x(String @REPLACE_HASH@a1xa, int @REPLACE_HASH@a1xb, char @REPLACE_HASH@a1xc,
			boolean @REPLACE_HASH@a1xd);

	/**
	 * removeEndPaddingFromString
	 * 
	 * @param stringToClean
	 * @param paddingCharachter
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1y(String @REPLACE_HASH@a1ya, char @REPLACE_HASH@a1yb);

	/**
	 * removeMultipleWhiteSpaceCharachters
	 * 
	 * @param string
	 * @return
	 */
	public abstract String @REPLACE_HASH@a1z(String @REPLACE_HASH@a1za);
}

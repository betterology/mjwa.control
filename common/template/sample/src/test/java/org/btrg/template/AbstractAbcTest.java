package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This comment needs to be fixed, but right now not sure how or what
 * @author petecarapetyan
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/bundle-context.xml" })
public abstract class Abstract@REPLACE_CLASS_NAME@Test{
}

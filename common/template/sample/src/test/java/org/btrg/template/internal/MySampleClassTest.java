package @REPLACE_PACKAGE@.@REPLACE_HASH@.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.internal.MySampleClass;
import @REPLACE_PACKAGE@.@REPLACE_HASH@.Abstract@REPLACE_CLASS_NAME@Test;
import org.junit.Before;
import org.junit.Test;

public class MySampleClassTest extends Abstract@REPLACE_CLASS_NAME@Test {
	private MySampleClass a;

	@Before
	public void setUp() throws Exception {
		a = new MySampleClass();
	}

	@Test
	public void testGetFrontFilledFixedLengthString() {
		assertEquals("aaa124", a.getFrontFilledFixedLengthString("124", 'a', 6,
				false, false));
		assertEquals("aaamybigDog", a.getFrontFilledFixedLengthString(
				"mybigDog", 'a', 11, false, false));
		try {
			assertEquals("aaamybigDog", a.getFrontFilledFixedLengthString(
					"mybigDog", 'a', 11, false, true));
			fail("Should have thrown NumberFormatException");
		} catch (NumberFormatException e) {
			// good
		}
		try {
			assertEquals("mybigDog", a.getFrontFilledFixedLengthString(
					"mybigDog", 'a', 6, true, false));
			fail("Should have thrown IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			// good
		}
		assertEquals(25, a.getFrontFilledFixedLengthString("124", '0', 25,
				false, false).length());
	}

	@Test
	public void testDotPathToSlashPath() {
		assertEquals(
				"C:/Users/petecarapetyan/AppData/Local/Mozilla/Firefox/MozillaFirefox/updates/0",
				a
						.dotPathToSlashPath("C:.Users.petecarapetyan.AppData.Local.Mozilla.Firefox.MozillaFirefox.updates.0"));
	}

	@Test
	public void testNotNull() {
		assertNotNull(a.notNull(null));
	}

	@Test
	public void testLastToken() {
		assertEquals("yo", a.lastToken("foo.me.wah.hoo.yo", '.'));
	}

	@Test
	public void testGetClassNameLikeFromDelimited() {
		assertEquals("MyBigDog", a.getClassNameLikeFromDelimited("my big dog",
				' '));
	}

	@Test
	public void testGetMethodNameLikeFromDelimited() {
		assertEquals("myBigDogFrank", a.getMethodNameLikeFromDelimited(
				"my big dog Frank", ' '));
	}

	@Test
	public void testNumberToLetter() throws IllegalArgumentException {
		try {
			a.numberToLetter(124, false);
			fail("Should have thrown IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().contains("out of the proper range"));
		}
	}

	@Test
	public void testReplace() {
		assertEquals("fbb", a.replace("foo", "o", "b"));

	}

	@Test
	public void testUpLow() {
		assertEquals("Mybigdog", a.upLow("mybigdog"));
		assertNotSame("Mybigdog", a.upLow("mybigDog"));
	}

	@Test
	public void testUpStart() {
		assertEquals("Mybigdog", a.upStart("mybigdog"));
		assertEquals("MybigDog", a.upStart("mybigDog"));
	}

	@Test
	public void testLowStart() {
		assertEquals("mybigdog", a.lowStart("mybigdog"));
		assertEquals("mybigDog", a.lowStart("mybigDog"));
	}

	@Test
	public void testTrimJavaPeteCStyle() {
		String lineWithTab = "my\tline\twith\ttabs";
		assertEquals("my  line  with  tabs", a.trimJavaPeteCStyle(lineWithTab));
		String lineWithTrailingSpace = "foo bar wah hoo yada       ";
		assertEquals("foo bar wah hoo yada", a
				.trimJavaPeteCStyle(lineWithTrailingSpace));
	}

	@Test
	public void testTrueTest() {
		assertTrue(a.trueTest("true"));
		assertTrue(a.trueTest("True"));
		assertTrue(a.trueTest("yes"));
		assertTrue(a.trueTest("Y"));
		assertTrue(a.trueTest("checked"));
	}

	@Test
	public void testYesNoToTrueFalse() {
		assertEquals("true", a.yesNoToTrueFalse("Y"));
		assertEquals("true", a.yesNoToTrueFalse("yes"));
		assertEquals("false", a.yesNoToTrueFalse("N"));
		assertEquals("false", a.yesNoToTrueFalse("no"));
	}

	@Test
	public void testListFromDelimited() {
		List<String> list = a.listFromDelimited("foo|bar|wah", "|");
		assertEquals(3, list.size());
		assertTrue(list.contains("foo"));
	}

	@Test
	public void testFalseTest() {
		assertTrue(a.falseTest("false"));
		assertTrue(a.falseTest("False"));
		assertTrue(a.falseTest("no"));
		assertTrue(a.falseTest("N"));
	}

	@Test
	public void testBooleanStringReturn() {
		assertEquals("true", a.booleanStringReturn("true"));
		assertEquals("true", a.booleanStringReturn("True"));
		assertEquals("true", a.booleanStringReturn("yes"));
		assertEquals("true", a.booleanStringReturn("Y"));
		assertEquals("true", a.booleanStringReturn("checked"));
		assertEquals("false", a.booleanStringReturn("false"));
		assertEquals("false", a.booleanStringReturn("False"));
		assertEquals("false", a.booleanStringReturn("no"));
		assertEquals("false", a.booleanStringReturn("N"));
		assertEquals("neither", a.booleanStringReturn("you got a lotta nerve"));
	}

	@Test
	public void testIsNull() {
		assertTrue(a.isNull(null));
		assertTrue(a.isNull(""));
		assertTrue(a.isNull(" "));
		assertFalse(a.isNull(" hi"));
	}

	@Test
	public void testSplitStringList() {
		List<String> list = a
				.splitStringList(
						"Every time I write something that is too long, it seems to me like it wants to wrap all over "
								+ "the place and just extend on out to the end of the page. This means it is pretty hard to read at times.",
						30);
		assertEquals("is too long, it seems to me like it", list.get(1));
	}

	@Test
	public void testSplitStringListDelimiter() {
		List<String> list = a
				.splitStringListDelimiter(
						"Every time I write something that is too long, it seems to me like it wants to wrap all over "
								+ "the place and just extend on out to the end of the page. This means it is pretty hard to read at times.",
						30, " ");
		assertEquals("is too long, it seems to me like it", list.get(1));
	}

	@Test
	public void testSplitString() {
		assertTrue(a
				.splitString(
						"Every time I write something that is too long, it seems to me like it wants to wrap all over "
								+ "the place and just extend on out to the end of the page. This means it is pretty hard to read at times.",
						30)
				.contains("\nwants to wrap all over the place and\n"));
	}

	@Test
	public void testSplitStringWithDelimiter() {
		/*
		 * This method is not clear to me why it was written so no test is
		 * created. Seems like a duplicate method
		 */
	}

	@Test
	public void testSplitStringForcedDelimiter() {
		/*
		 * This method was written for strings with different delimiters of some
		 * sort but I cannot imagine the use case or even how to call this
		 * method. So I am not writing the test now. I know that there was
		 * actually a very good use case, just can't remember it.
		 */
	}

	@Test
	public void testRandomString() {
		String one = a.randomString(13, false);
		String two = a.randomString(13, false);
		assertTrue(13 == one.length());
		assertNotSame(one, two);
	}

	@Test
	public void testRandomNumberString() {
		String numericString = a.randomNumberString(8);
		assertTrue(8 == numericString.length());
		Integer.valueOf(numericString);
	}

	@Test
	public void testDatePrint() {
		assertTrue(a.datePrint().startsWith("20"));
		assertTrue(10 == a.datePrint().length());
	}

	@Test
	public void testYyMMddHHmmssPrint() {
		assertTrue(a.yyMMddHHmmssPrint().startsWith("1"));
		assertTrue(12 == a.yyMMddHHmmssPrint().length());
	}

	@Test
	public void testYyyy_MM_dd() {
		assertTrue(a.yyyy_MM_dd(new Date()).startsWith("201"));
		assertTrue(4 == a.yyyy_MM_dd(new Date()).indexOf("_"));
	}

	@Test
	public void testYyyyDashMMDashdd() {
		assertTrue(a.yyyyDashMMDashdd(new Date()).startsWith("201"));
		assertTrue(4 == a.yyyyDashMMDashdd(new Date()).indexOf("-"));
	}

	@Test
	public void testDd_MMM_yy() {
		assertTrue(9 == a.dd_MMM_yy(new Date()).length());
		assertTrue(2 == a.dd_MMM_yy(new Date()).indexOf("-"));
	}

	@Test
	public void testYyyyMMdd() {
		assertTrue(a.yyyyMMdd(new Date()).startsWith("201"));
		assertTrue(8 == a.yyyyMMdd(new Date()).length());
		assertTrue(-1 == a.yyyyMMdd(new Date()).indexOf("-"));

	}

	@Test
	public void testYyMMddHHmmss() {
		assertTrue(a.yyMMddHHmmss(new Date()).startsWith("1"));
		assertTrue(12 == a.yyMMddHHmmss(new Date()).length());
		assertTrue(-1 == a.yyMMddHHmmss(new Date()).indexOf("-"));
	}

	@Test
	public void testYyMMdd() {
		assertTrue(6 == a.yyMMdd(new Date()).length());
		assertTrue(-1 == a.yyMMdd(new Date()).indexOf("-"));
		assertTrue(-1 == a.yyMMdd(new Date()).indexOf("_"));
	}

	@Test
	public void testmMslashddslashyyyy() {
		assertTrue(10 == a.mMslashddslashyyyy(new Date()).length());
		assertTrue(2 == a.mMslashddslashyyyy(new Date()).indexOf("/"));
		assertTrue(-1 == a.mMslashddslashyyyy(new Date()).indexOf("_"));
		assertEquals("201", a.mMslashddslashyyyy(new Date()).substring(6, 9));
	}

	@Test
	public void testYyyyMMddHHmmss() {
		assertTrue(a.yyyyMMddHHmmss(new Date()).startsWith("201"));
		assertTrue(14 == a.yyyyMMddHHmmss(new Date()).length());
		assertTrue(-1 == a.yyyyMMddHHmmss(new Date()).indexOf("/"));
		assertTrue(-1 == a.yyyyMMddHHmmss(new Date()).indexOf("_"));
		assertTrue(-1 == a.yyyyMMddHHmmss(new Date()).indexOf("-"));
	}

	@Test
	public void testEndSingleSlash() {
		assertEquals("/foo/bar/wah/", a.endSingleSlash("/foo/bar/wah"));
		assertEquals("\\foo\\bar\\wah/", a.endSingleSlash("\\foo\\bar\\wah\\"));
		assertEquals("\\foo\\bar\\wah/", a
				.endSingleSlash("\\foo\\bar\\wah\\\\\\"));
		assertEquals("/foo/bar/wah/", a.endSingleSlash("/foo/bar/wah////"));
	}

	@Test
	public void testRemoveLineHavingValue() {
		String myThreeLines = "the cow jumped over the moon\n"
				+ "the doctor blessed the event\n"
				+ "the spaceship invaded as a consequence";
		assertFalse(a.removeLineHavingValue(myThreeLines, "doctor").contains(
				"doctor"));
		assertTrue(a.removeLineHavingValue(myThreeLines, "cow").startsWith(
				"the doctor"));
	}

	@Test
	public void testRemoveEmptyLines() {
		String myThreeLinesWithoutSpace = "the cow jumped over the moon\n"
				+ "the doctor blessed the event\n"
				+ "the spaceship invaded as a consequence";
		String myThreeLinesWithExtra = "the cow jumped over the moon\n\n"
				+ "the doctor blessed the event\n\n\n"
				+ "the spaceship invaded as a consequence";
		assertEquals(myThreeLinesWithoutSpace, a
				.removeEmptyLines(myThreeLinesWithExtra));
	}

	@Test
	public void testOrderLinesByAlpha() {
		String myThreeLinesOutOfOrder = "the zippy cow jumped over the moon\n"
				+ "the doctor blessed the event\n"
				+ "the spaceship invaded as a consequence";
		String myThreeLinesInOrder = "the doctor blessed the event\n"
				+ "the spaceship invaded as a consequence\n"
				+ "the zippy cow jumped over the moon";
		String result = a.orderLinesByAlpha(myThreeLinesOutOfOrder);
		assertEquals(myThreeLinesInOrder, result);
	}

	@Test
	public void testAsciiIsInTextRange() {
		char letter = 'a';
		char symbol = '~';
		char number = '3';
		assertTrue(a.asciiIsInTextRange(letter, false));
		assertFalse(a.asciiIsInTextRange(symbol, false));
		assertFalse(a.asciiIsInTextRange(number, false));
		assertFalse(a.asciiIsInTextRange(symbol, true));
	}

	@Test
	public void testLoadConvert() {
		// have no idea what this does, so I won't even try to test it. Looks
		// important though!
	}

	@Test
	public void testShift6() {
		String shiftThis = "abcdefgxyz123789ABCDEFGXYZ";
		String shifted6 = "ghijklm~ 789=>?GHIJKLM^_`";
		assertEquals(shifted6, a.shift6(shiftThis));
	}

	@Test
	public void testShift6back() {
		String shiftThis = "abcdefgxyz123789ABCDEFGXYZ";
		String shifted6 = "ghijklm~ 789=>?GHIJKLM^_`";
		assertEquals(shiftThis, a.shift6back(shifted6));
	}

	@Test
	public void testGetPaddedString() {
		assertEquals("124aaa", a.getPaddedString("124", 6, 'a', false));
		assertEquals("124aevv", a.getPaddedString("124aevv", 3, 'a', true));
		assertEquals("mybigDogaaa", a.getPaddedString("mybigDog", 11, 'a',
				false));
	}

	@Test
	public void testGetPrePaddedString() {
		assertEquals("aaa124", a.getPrePaddedString("124", 6, 'a', false));
		assertEquals("124789455", a.getPrePaddedString("124789455", 6, 'a',
				true));
	}

	@Test
	public void testRemoveEndPaddingFromString() {
		assertEquals("124", a.removeEndPaddingFromString("124aa", 'a'));
	}

	@Test
	public void testRemoveMultipleWhiteSpaceCharachters() {
		String after = "the cow jumped over the moon";
		String before = "the   cow\t jumped  \fover \nthe moon";
		assertEquals(after, a.removeMultipleWhiteSpaceCharachters(before));
	}

	@Test
	public void testIsWhiteSpace() {
		assertTrue(a.isWhiteSpace('\n'));
		assertTrue(a.isWhiteSpace('\t'));
		assertTrue(a.isWhiteSpace(' '));
		assertTrue(a.isWhiteSpace('\r'));
		assertTrue(a.isWhiteSpace('\f'));
	}

}


package @REPLACE_PACKAGE@.@REPLACE_HASH@;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a;
import @REPLACE_PACKAGE@.@REPLACE_HASH@.@REPLACE_CLASS_NAME@a;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * This comment needs to be fixed, but right now not sure how or what
 * @author petecarapetyan
 *
 */
public class @REPLACE_CLASS_NAME@aTest extends Abstract@REPLACE_CLASS_NAME@Test{
	@Autowired
	I@REPLACE_CLASS_NAME@a @REPLACE_HASH@a;


	@Before
	public void setUp() throws Exception {
		System.out.println("Yes, setup ran");
	}

	@Test
	public void myTest() throws Exception {
		assertTrue(true);
		assertNotNull(@REPLACE_HASH@a.@REPLACE_HASH@a1a(6, false));
		assertNotNull(@REPLACE_HASH@a.@REPLACE_HASH@a1a(6, false));
		System.out.println(@REPLACE_HASH@a.@REPLACE_HASH@a1c());
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Yes, tearDown ran");
	}


    /**
     * absolutely useless as a test, needs to be a real test, in real life
     */
	@Test
    public void testBeanIsABean(){
    	@REPLACE_CLASS_NAME@a @REPLACE_HASH@a = new @REPLACE_CLASS_NAME@a();
		assertNotNull(@REPLACE_HASH@a.@REPLACE_HASH@a1a(6, false));
    }

	@Test
	public void testAsciiIsInTextRange() {
		char letter = 'a';
		char symbol = '~';
		char number = '3';
		assertTrue(@REPLACE_HASH@a.@REPLACE_HASH@a1s(letter, false));
		assertFalse(@REPLACE_HASH@a.@REPLACE_HASH@a1s(symbol, false));
		assertFalse(@REPLACE_HASH@a.@REPLACE_HASH@a1s(number, false));
		assertFalse(@REPLACE_HASH@a.@REPLACE_HASH@a1s(symbol, true));
	}
}

            ------
            How to obfuscate the public @REPLACE_PACKAGE@.@REPLACE_HASH@ API:
            ------
            ------
             2010-04-26
            ------

How to obfuscate the public @REPLACE_PACKAGE@.@REPLACE_HASH@ API:

* A Caution:

  What follows is how we try to make the best out of a bad situation, explained below.
  
  
  You may conclude that the cure is worse than the problem, but <<you have no choice but to follow these procedures if you wish to develop in this module>>. 


* Why Make It Harder Than It Has To Be?

  Betterology Corporation wants to be able to pay developers, even in a crazy world where most java code written is either free, or else reverse engineer-able. The reverse engineering problem is especially problematic for us, in that we have chosen to deploy client side java, rather than server side java. 
  
  
  In order to accomplish this, we protect our intellectual property as much as is practical or reasonable.
  
  
  Practical, in this case, means we protect code through obfuscation first, and through legal means only as a last resort.  
  
* Practicality, Obfuscation, and OSGI:

  Since obfuscation of code is totally automated through proguard, that leaves only interfaces that may not be obfuscated through automated means.


  We use OSGI between modules, and automated obfuscation within modules.
  
  
  What this means is that we can't obfuscate calls between OSGI bundles, so you have to do that manually.
  
  
  What follows is 
  
  * How you will write code that obfuscates published interfaces.
  
  * How you will, in turn, make that obfuscated published interface somewhat usable.
  
  * How you package the rest of the automated 
  
* The Good News - Only One Thing To Obfuscate Manually:

  The good news is that the ONLY thing you have to obfuscate manually is this module's external interface to the outside world.
  
  
  We want to keep that interface simple anyway, so hopefully there won't be much that follows this pattern.
  
  
  Everything that is not an external interface follows normal coding procedures with only package name restrictions. See below on private package naming restrictions.

* Public Interfaces: @REPLACE_PACKAGE@.@REPLACE_HASH@

  No api is published through any other package!  
  
  Use this, and only this package, for any interfaces which are published to the outside world.
  
  ONLY INTERFACES (not concrete classes) may be published under the @REPLACE_PACKAGE@.@REPLACE_HASH@ package!
  
    
* Public Class Names: @REPLACE_CLASS_NAME@[a]

  public @REPLACE_HASH@ class names are always 4 digits
  


  Examples of allowable class names:
  
  * @REPLACE_CLASS_NAME@a.java
  
  * @REPLACE_CLASS_NAME@b.java
  
  * @REPLACE_CLASS_NAME@c.java
  
  * @REPLACE_CLASS_NAME@d.java
  
  * etc
  
  
* Public Class Methods

  public class methods are always 5 digits. The first 4 digits repeat the signature of the class name.
  
  
  
  Example: Given the classname of @REPLACE_CLASS_NAME@c.java, the allowable public method signatures would be:
  
  * @REPLACE_HASH@ca
  
  * @REPLACE_HASH@cb
  
  * @REPLACE_HASH@cc
  
  * @REPLACE_HASH@cd
  
  * etc
  
  
* Public Class Method Parameters

  public class method parameters are always 6 digits. The first 5 digits repeat the signature of the method name.
  
  Example: Given the method of @REPLACE_CLASS_NAME@ce, the allowable public parameter names would be:
  
  * @REPLACE_HASH@cea
  
  * @REPLACE_HASH@ceb
  
  * @REPLACE_HASH@cec
  
  * @REPLACE_HASH@ced
  
  * etc
  

* Real Example Code:

  If your external interface's first method had incoming parameters of two strings and returned an int, here would be <<the entire interface>>.


+-------------------------------+
  package @REPLACE_PACKAGE@.@REPLACE_HASH@;
  
  public interface @REPLACE_CLASS_NAME@a {
  
    public int @REPLACE_HASH@aa(String @REPLACE_HASH@aaa, String @REPLACE_HASH@aab);
    
  }
+-------------------------------+
  

  
  
  If that doesn't drive any good developer totally nuts, well, it should. The exact opposite of rule number 1, which is to write self documenting code. This code says absolutely nothing.
  
* JavaDOC at the Interface Level:

  It is a courteosy to all future developers on this module that you would make very clear javadoc at the interface level. 
  
  Why? Javadoc is not copied into the compiled class file, and thus is not reverse-engineer-able using something like JAD
  
  What follows is the same class as above, only with javaDoc added:
  
+-------------------------------+

  package @REPLACE_PACKAGE@.@REPLACE_HASH@;
  
 /** 
 * @REPLACE_CLASS_NAME@ serves the very important function of cleaning up Wahoo files by taking their contents
 * and doing analysis on them according to Wahooian statistical analysis methods. 
 *  
 * @author petecarapetyan 
 */
 
  public interface @REPLACE_CLASS_NAME@a \{
  	
	/**	
	 * @param @REPLACE_HASH@aa file type	 
	 * @param @REPLACE_HASH@ab file contents	 
	 * @return Wahooian statistical count reference	 
	 */
	 
    public int @REPLACE_HASH@aa(String @REPLACE_HASH@aaa, String @REPLACE_HASH@aab);
    
  }
+-------------------------------+

  

* Private Package Naming Restrictions:

  Any package that is not in the public interface must begin with the prefix @REPLACE_HASH@
  
  Thus, acceptable package names for this module might be
  
  * @REPLACE_HASH@.wahoo.foo.bar
  
  * @REPLACE_HASH@.wahoo.domain
  
  * @REPLACE_HASH@.foo.any.thing.you.want
  
  * @REPLACE_HASH@.bar.foo.model
  
  * etc
  
  
  
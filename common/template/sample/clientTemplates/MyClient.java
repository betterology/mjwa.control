package com.foo.bar;

import @REPLACE_PACKAGE@.@REPLACE_HASH@.I@REPLACE_CLASS_NAME@a;

/**
 * example of code that might call I@REPLACE_CLASS_NAME@a
 */
@Component
public class MyClient {
	private I@REPLACE_CLASS_NAME@a @REPLACE_HASH@a;
	
	@Autowired
	MyClient(I@REPLACE_CLASS_NAME@a @REPLACE_HASH@a){
		this.@REPLACE_HASH@a = @REPLACE_HASH@a;
	}
	
}
